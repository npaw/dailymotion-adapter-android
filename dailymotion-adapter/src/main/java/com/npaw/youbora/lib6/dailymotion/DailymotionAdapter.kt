package com.npaw.youbora.lib6.dailymotion

import com.dailymotion.android.player.sdk.PlayerWebView
import com.dailymotion.android.player.sdk.events.*

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.adapter.PlayerAdapter

open class DailymotionAdapter(player: PlayerWebView) : PlayerAdapter<PlayerWebView>(player) {

    var listener: PlayerWebView.EventListener? = null

    private val myListener = object : PlayerWebView.EventListener {

        override fun onEventReceived(event: PlayerEvent) {
            if (event !is TimeUpdateEvent && event !is ProgressEvent && event !is AdTimeUpdateEvent)
                YouboraLog.debug("PlayerEvent: ${event.name}")

            when (event) {
                is StartEvent -> plugin?.adsAdapter = getAdAdapter()
                is VideoStartEvent -> {
                    plugin?.adsAdapter?.fireAdBreakStop()
                    fireStart()
                }

                is PlayingEvent -> {
                    fireJoin()
                    monitor?.skipNextTick()
                }

                is SeekingEvent -> fireSeekBegin()
                is SeekedEvent -> {
                    fireSeekEnd()
                    monitor?.skipNextTick()
                }

                is EndEvent -> fireStop()
                is PauseEvent -> firePause()
                is PlayEvent -> {
                    fireResume()
                    fireStart()
                }

                is ErrorEvent -> fireFatalError()
            }

            listener?.onEventReceived(event)
        }
    }

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()

        monitorPlayhead(monitorBuffers = true, monitorSeeks = false, interval = 800)

        player?.setEventListener(myListener)
    }

    override fun unregisterListeners() {
        listener?.let { player?.setEventListener(it) }
        super.unregisterListeners()
    }

    override fun getPlayhead(): Double? {
        return player?.position?.let { it / 1000.0 }
    }

    override fun getTitle(): String? { return player?.title }
    override fun getResource(): String? { return player?.url }
    override fun getPlayerName(): String { return "Dailymotion" }
    override fun getDuration(): Double? { return player?.duration }
    override fun getRendition(): String? { return player?.quality }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }

    private fun getAdAdapter(): DailymotionAdAdapter {
        return DailymotionAdAdapter(player, myListener)
    }
}