package com.npaw.youbora.lib6.dailymotion

import com.dailymotion.android.player.sdk.PlayerWebView
import com.dailymotion.android.player.sdk.events.*

import com.npaw.youbora.lib6.adapter.AdAdapter

class DailymotionAdAdapter(player: PlayerWebView?, val listener: PlayerWebView.EventListener) :
    AdAdapter<PlayerWebView>(player) {

    private val myListener = object : PlayerWebView.EventListener {

        override fun onEventReceived(event: PlayerEvent) {

            when (event) {
                is AdStartEvent -> {
                    plugin?.adapter?.firePause()
                    fireStart()
                }

                is AdPlayEvent -> {
                    fireResume()
                    fireJoin()
                }

                is AdPauseEvent -> {
                    firePause()
                }

                is AdEndEvent -> fireStop()
                //is AdTimeUpdateEvent ->
            }

            listener.onEventReceived(event)
        }
    }

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()

        monitorPlayhead(monitorBuffers = true, monitorSeeks = false, interval = 800)

        player?.setEventListener(myListener)
    }

    override fun unregisterListeners() {
        player?.setEventListener(listener)
        super.unregisterListeners()
    }

    override fun getPlayerName(): String { return "Dailymotion" }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }
}