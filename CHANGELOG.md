## [6.7.7] - 2021-06-21
### Added
- Ad buffer.

## [6.7.6] - 2021-06-21
### Added
- Ad pause.

## [6.7.5] - 2021-06-15
### Added
- Error reporting.

## [6.7.4] - 2021-05-27
### Added
- Ad adapter.

## [6.7.3] - 2021-05-21
### Added
- Player event logs.

## [6.7.2] - 2021-05-19
### Added
- Dailymotion SDK version 0.2.x support.

## [6.7.1] - 2021-05-13
### Modified
- Deployment platform moved from Bintray to JFrog.

## [6.7.0] - 2020-03-05
### Refactored
- Adapter to work with the new Youboralib version.

## [6.5.0] - 2019-10-17
- First release.